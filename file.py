import pandas as pd # импорт библиотеки pandas
import numpy as np # импорт библиотеки numpy
import matplotlib.pyplot as plt # импорт модуля pyplot для работы с графиками
from sklearn.model_selection import train_test_split # импорт training_test_split
from sklearn.feature_extraction.text import CountVectorizer # импорт CountVectorizer

df = pd.read_csv("Train_rev1.csv", delimiter = ",") # формирование dataframe из файла Train_rev1.csv
print (df.shape) # вывод количества строк и столбцов в dataframe
df.head() # вывод первых 5 строк dataframe

median = np.median(df["SalaryNormalized"]) # нахождение медианы выборки SalaryNormalized
plt.figure(figsize = (16,8)) # установка размеров области размещения графика
plt.hist(df["SalaryNormalized"], bins = 50) # установка вида диаграммы - гистограмма, с количеством столбцов (равной ширины) равным 50
plt.axvline(median, c = "purple") # отрисовка медианы и установка её цвета в красный (color = "red")
plt.xlabel("salary", fontsize = 20) # установка наименования оси абсцисс и размера шрифта подписи 
plt.ylabel("count", fontsize = 20) # установка наименования оси ординат и размера шрифта подписи
plt.grid # построение диаграммы

df["SalaryNormalized"] = (df["SalaryNormalized"] > median).astype(int) # бинаризация признака SalaryNormalized по порогу median
df.drop("SalaryRaw", axis = 1, inplace = True) # удаление столбца SalaryRaw с внесением в исходный dataframe
df.head() # вывод первых 5 строк dataframe

training_sample, test_sample = train_test_split(df, train_size = 0.7, random_state = 42, shuffle = True) # разбиение выборки на обучающую и контрольную в соотношении 70/30 с использованием перемешивания объектов
print (training_sample.shape) # вывод структуры training_sample
training_sample.head() # вывод первых 5 строк training_sample
print (test_sample.shape) # вывод структуры test_sample
test_sample.head() # вывод первых 5 строк test_sample


cols = ["Title", "LocationNormalized", "ContractType", "ContractTime", "Company"]

def description(sample):
    result = []
    for index, row in sample.iterrows():
        temp = ""
        for col in cols:
           temp += " " + str(row[col])
        result.append(temp)
    return result

description_df = description(df)
description_training_sample = description(training_sample)
description_test_sample = description(test_sample)
        
cv = CountVectorizer() # инициализация CountVectorizer
count_vector = cv.fit_transform(description_df) # подсчёт структуры
print (cv.get_feature_names()) # получение словаря, сгенерированного на основе подсчитанной структуры
print(count_vector.toarray()) # вывод матрицы
count_vector_training_sample = cv.transform(description_training_sample) # подсчёт структуры
count_vector_test_sample = cv.transform(description_test_sample) # подсчёт структуры
print(count_vector_training_sample.toarray()) # вывод матрицы
print(count_vector_test_sample.toarray()) # вывод матрицы